<?php
$TRANSLATIONS = array(
"Chat" => "Чат",
"Search in conversations" => "Търси в разговори",
"Add Person" => "Добави Човек",
"Chat Message" => "Чат Съобщение",
"Search in users" => "Търси сред потребителите",
"There are no other users on this ownCloud." => "Няма други потребители в този ownCloud.",
"In order to chat please create at least one user, it will appear on the left." => "Моля, създай поне един потребител, за да можеш да чатваш. Той ще се появи в ляво."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
