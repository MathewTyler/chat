<?php
$TRANSLATIONS = array(
"Chat" => "Chat",
"{displayname} attached {path} to this conversation" => "{displayname} přiložil(a) {path} k této konverzaci",
"{displayname} removed {path} from this conversation" => "{displayname} odstranil(a) {path} z této konverzace",
"Search in conversations" => "Vyhledat v konverzacích",
"Add Person" => "Přidat osobu",
"View Attached files" => "Zobrazit přiložené soubory",
"Chat Message" => "Zpráva chatu",
"Files attached to this conversation" => "Soubory přiložené k této konverzaci",
"Download " => "Stáhnout",
"Attach more files" => "Přiložit více souborů",
"Search in users" => "Hledat mezi uživateli",
"There are no other users on this ownCloud." => "Na tomto ownCloud serveru nejsou žádní jiní uživatelé.",
"In order to chat please create at least one user, it will appear on the left." => "Pro fungování chatu je třeba mít vytvořené alespoň jedno uživatelské jméno, které se poté objeví na panelu vlevo."
);
$PLURAL_FORMS = "nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;";
