<?php
$TRANSLATIONS = array(
"Chat" => "Chat",
"{displayname} attached {path} to this conversation" => "{displayname} vedhæftede {path} til denne samtale",
"{displayname} removed {path} from this conversation" => "{displayname} fjernede {path} fra denne samtale",
"Search in conversations" => "Søg i konversationer",
"Add Person" => "Tilføj person",
"View Attached files" => "Vis vedhæftede filer",
"Chat Message" => "Chat-besked",
"Files attached to this conversation" => "Filer som er vedhæftet til denne samtale",
"Download " => "Download ",
"Attach more files" => "Vedhæft flere filer",
"Search in users" => "Søg i brugere",
"There are no other users on this ownCloud." => "Der er ingen andre brugere i denne ownCloud.",
"In order to chat please create at least one user, it will appear on the left." => "For at gøre brug af chatten skal der mindst oprettes én bruger, der vil blive vist i venstre side."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
