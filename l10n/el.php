<?php
$TRANSLATIONS = array(
"Chat" => "Συνομιλία",
"Search in conversations" => "Αναζήτηση στις συζητήσεις",
"Add Person" => "Προσθήκη Ατόμου",
"Chat Message" => "Μήνυμα Συνομιλίας",
"Search in users" => "Αναζήτηση χρηστών",
"There are no other users on this ownCloud." => "Δεν υπάρχουν άλλοι χρήστες σε αυτό το ownCloud.",
"In order to chat please create at least one user, it will appear on the left." => "Για να συνομιλήσετε παρακαλώ δημιουργήστε τουλάχιστον ένα χρήστη, θα εμφανιστεί στα αριστερά."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
