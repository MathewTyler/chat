<?php
$TRANSLATIONS = array(
"Chat" => "Chat",
"{displayname} attached {path} to this conversation" => "{displayname} attached {path} to this conversation",
"{displayname} removed {path} from this conversation" => "{displayname} removed {path} from this conversation",
"Search in conversations" => "Search in conversations",
"Add Person" => "Add Person",
"View Attached files" => "View Attached files",
"Chat Message" => "Chat Message",
"Files attached to this conversation" => "Files attached to this conversation",
"Download " => "Download ",
"Attach more files" => "Attach more files",
"Search in users" => "Search in users",
"There are no other users on this ownCloud." => "There are no other users on this ownCloud.",
"In order to chat please create at least one user, it will appear on the left." => "In order to chat please create at least one user, it will appear on the left."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
