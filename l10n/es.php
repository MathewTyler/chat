<?php
$TRANSLATIONS = array(
"Chat" => "Chat",
"{displayname} attached {path} to this conversation" => "{displayname} adjuntó {path} a esta conversación",
"{displayname} removed {path} from this conversation" => "{displayname} eliminó {path} de esta conversación",
"Search in conversations" => "Buscar en las conversaciones",
"Add Person" => "Agregar persona",
"View Attached files" => "Ver archivos adjuntos",
"Chat Message" => "Mensaje",
"Files attached to this conversation" => "Archivos adjuntos de esta conversación",
"Download " => "Descargar",
"Attach more files" => "Adjuntar más archivos",
"Search in users" => "Buscar en usuarios",
"There are no other users on this ownCloud." => "No hay otros usuarios en esta ownCloud",
"In order to chat please create at least one user, it will appear on the left." => "Para poder chatear, debe crear por lo menos un usuario; éste aparecerá a la izquierda."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
