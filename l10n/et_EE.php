<?php
$TRANSLATIONS = array(
"Chat" => "Vestlus",
"{displayname} attached {path} to this conversation" => "{displayname} lisas sellele vestlsele {path}",
"{displayname} removed {path} from this conversation" => "{displayname} eemaldas sellest vestlsest {path}",
"Search in conversations" => "Otsi vestlustest",
"Add Person" => "Lisa inimene",
"View Attached files" => "Vaata lisatud faile",
"Chat Message" => "Vestluse sõnum",
"Files attached to this conversation" => "Sellele vestlusele lisatud failid",
"Download " => "Laadi alla",
"Attach more files" => "Lisa rohkem faile",
"Search in users" => "Otsi kasutajatest",
"There are no other users on this ownCloud." => "Selles ownCloudis pole ühtegi teist kasutajat.",
"In order to chat please create at least one user, it will appear on the left." => "Vestlemiseks loo palun vähemalt üks kasutaja. See on nähtav vasakul."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
