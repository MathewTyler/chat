<?php
$TRANSLATIONS = array(
"Chat" => "Berriketa",
"Search in conversations" => "Bilatu berriketetan",
"Add Person" => "Gehitu pertsona",
"Chat Message" => "Berriketa mezua",
"Search in users" => "Bilatu erabiltzaileetan",
"There are no other users on this ownCloud." => "Ez dago beste erabiltzailerik ownCloud honetan.",
"In order to chat please create at least one user, it will appear on the left." => "Berriketan aritzeko sortu gutxienez erabiltzaile bat, ezkerrean agertuko da."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
