<?php
$TRANSLATIONS = array(
"Chat" => "Keskustelu",
"{displayname} attached {path} to this conversation" => "{displayname} liitti kohteen {path} tähän keskusteluun",
"{displayname} removed {path} from this conversation" => "{displayname} poisti kohteen {path} tästä keskustelusta",
"Search in conversations" => "Etsi keskusteluista",
"Add Person" => "Lisää henkilö",
"View Attached files" => "Näytä liitetyt tiedostot",
"Chat Message" => "Viesti",
"Files attached to this conversation" => "Tähän keskusteluun liitetyt tiedostot",
"Download " => "Lataa",
"Attach more files" => "Lisää enemmän tiedostoja",
"Search in users" => "Etsi käyttäjistä",
"There are no other users on this ownCloud." => "Tässä ownCloudissa ei ole muita käyttäjiä.",
"In order to chat please create at least one user, it will appear on the left." => "Luo vähintään yksi käyttäjä, jotta voit keskustella. Käyttäjät ilmestyvät näkyviin vasemmalle."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
