<?php
$TRANSLATIONS = array(
"Chat" => "Discussion",
"Search in conversations" => "Rechercher dans les conversations",
"Add Person" => "Ajouter une personne",
"Chat Message" => "Message de discussion",
"Search in users" => "Rechercher dans les utilisateurs",
"There are no other users on this ownCloud." => "Aucun autre utilisateur n'est présent sur cet ownCloud.",
"In order to chat please create at least one user, it will appear on the left." => "Pour pouvoir lancer une discussion, veuillez créer au moins un utilisateur. Il aparaîtra sur la gauche."
);
$PLURAL_FORMS = "nplurals=2; plural=(n > 1);";
