<?php
$TRANSLATIONS = array(
"Chat" => "Conversas",
"Search in conversations" => "Buscar nas conversas",
"Add Person" => "Engadir unha persoa",
"Chat Message" => "Mensaxe da conversa",
"Search in users" => "Busca en usuarios",
"There are no other users on this ownCloud." => "Non hai outros usuarios neste ownCloud."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
