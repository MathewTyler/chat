<?php
$TRANSLATIONS = array(
"Chat" => "Chat",
"{displayname} attached {path} to this conversation" => "{displayname} ha allegato {path} a questa conversazione",
"{displayname} removed {path} from this conversation" => "{displayname} ha rimosso {path} da questa conversazione",
"Search in conversations" => "Cerca nelle conversazioni",
"Add Person" => "Aggiungi persona",
"View Attached files" => "Visualizza file allegati",
"Chat Message" => "Messaggio di chat",
"Files attached to this conversation" => "File allegati a questa conversazione",
"Download " => "Scarica",
"Attach more files" => "Allegato altri file",
"Search in users" => "Cerca tra gli utenti",
"There are no other users on this ownCloud." => "Non ci sono altri utenti in questo ownCloud.",
"In order to chat please create at least one user, it will appear on the left." => "Per iniziare una chat, crea almeno un utente, apparirà a sinistra."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
