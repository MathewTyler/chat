<?php
$TRANSLATIONS = array(
"Chat" => "チャット",
"Search in conversations" => "会話を検索",
"Add Person" => "人を追加",
"Chat Message" => "チャットメッセージ",
"Search in users" => "ユーザーを検索",
"There are no other users on this ownCloud." => "このownCloudには他のユーザーはいません。",
"In order to chat please create at least one user, it will appear on the left." => "チャットする場合には、最低1名ユーザーを追加してください。ユーザーは左側に表示されます。"
);
$PLURAL_FORMS = "nplurals=1; plural=0;";
