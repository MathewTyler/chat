<?php
$TRANSLATIONS = array(
"Chat" => "Chat",
"{displayname} attached {path} to this conversation" => "{displayname} koppelde {path} aan dit gesprek",
"{displayname} removed {path} from this conversation" => "{displayname} verwijderde {path} uit dit gesprek",
"Search in conversations" => "Zoeken in gesprekken",
"Add Person" => "Toevoegen persoon",
"View Attached files" => "Bekijk gekoppelde bestanden",
"Chat Message" => "Bericht",
"Files attached to this conversation" => "Bestanden gekoppeld aan dit gesprek",
"Download " => "Download ",
"Attach more files" => "Koppel meer bestanden",
"Search in users" => "Zoek binnen gebruikers",
"There are no other users on this ownCloud." => "Er zijn geen andere gebruikers in deze ownCloud",
"In order to chat please create at least one user, it will appear on the left." => "Om te chatten, moet u minimaal één gebruiker aanmaken; deze verschijnt links."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
