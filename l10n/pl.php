<?php
$TRANSLATIONS = array(
"Chat" => "Czat",
"Search in conversations" => "Szukaj w rozmowach",
"Add Person" => "Dodaj osobę",
"Chat Message" => "Wiadomość czatu",
"Search in users" => "Szukaj wśród użytkowników",
"There are no other users on this ownCloud." => "Nie istnieją inni użytkownicy na tym ownCloud-zie",
"In order to chat please create at least one user, it will appear on the left." => "Aby używać chat-a, utwórz przynajmniej jednego użytkownika. Pojawi się po lewej stronie"
);
$PLURAL_FORMS = "nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);";
