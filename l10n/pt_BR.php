<?php
$TRANSLATIONS = array(
"Chat" => "Bate papo",
"{displayname} attached {path} to this conversation" => "{displayname} anexado {path} a esta conversa",
"{displayname} removed {path} from this conversation" => "{displayname} removido {path} desta conversa",
"Search in conversations" => "Pesquisar em conversas",
"Add Person" => "Adicionar uma Pessoa",
"View Attached files" => "Visualizar Arquivos em Anexo",
"Chat Message" => "Mensagem de Bate-Papo",
"Files attached to this conversation" => "Arquivos anexados a esta conversa",
"Download " => "Baixar",
"Attach more files" => "Anexar mais arquivos",
"Search in users" => "Pesquisar em usuários",
"There are no other users on this ownCloud." => "Não há outros usuários neste ownCloud.",
"In order to chat please create at least one user, it will appear on the left." => "Para bater papo por favor crie pelo menos um usuário, isto irá aparecer a esquerda."
);
$PLURAL_FORMS = "nplurals=2; plural=(n > 1);";
