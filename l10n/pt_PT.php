<?php
$TRANSLATIONS = array(
"Chat" => "Conversar",
"Search in conversations" => "Pesquisar em conversações",
"Add Person" => "Adicionar Pessoa",
"Chat Message" => "Mensagem",
"Search in users" => "Pesquisar em utilizadores",
"There are no other users on this ownCloud." => "Não existem mais utilizadores nesta ownCloud",
"In order to chat please create at least one user, it will appear on the left." => "\tPara poder conversar crie por favor pelo menos um utilizador, irá aparecer na lista á direita."
);
$PLURAL_FORMS = "nplurals=2; plural=(n != 1);";
