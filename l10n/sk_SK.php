<?php
$TRANSLATIONS = array(
"Chat" => "Chat",
"Search in conversations" => "Vyhľadať v konverzáciách",
"Add Person" => "Pridať osobu",
"Chat Message" => "Správa",
"Search in users" => "Vyhľadať v používateľoch",
"There are no other users on this ownCloud." => "Na tomto ownCloude nie sú iní používatelia.",
"In order to chat please create at least one user, it will appear on the left." => "Na správne fungovanie chatu je potrebné mať vytvorené aspoň jedno používateľské meno, ktoré sa neskôr objaví na paneli vľavo."
);
$PLURAL_FORMS = "nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;";
