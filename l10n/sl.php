<?php
$TRANSLATIONS = array(
"Chat" => "Klepet",
"{displayname} attached {path} to this conversation" => "Uporabnik {displayname} je priložil {path} v pogovor",
"{displayname} removed {path} from this conversation" => "Uporabnik {displayname} je odstranil {path} iz pogovor",
"Search in conversations" => "Poišči v pogovorih",
"Add Person" => "Dodaj osebo",
"View Attached files" => "Poglej priložene datoteke",
"Chat Message" => "Sporočilo klepetanja",
"Files attached to this conversation" => "Datoteke, priložene temu pogovoru",
"Download " => "Prejmi",
"Attach more files" => "Priloži več datotek",
"Search in users" => "Poišči med uporabniki",
"There are no other users on this ownCloud." => "Ni drugih uporabnikov oblaka ownCloud.",
"In order to chat please create at least one user, it will appear on the left." => "Za klepet je treba ustvariti vsaj enega uporabnika. Njegovo ime bo prikazano na levi."
);
$PLURAL_FORMS = "nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);";
