<?php
$TRANSLATIONS = array(
"Chat" => "Sohbet",
"{displayname} attached {path} to this conversation" => "{displayname}, bu konuşmaya {path} ekini yerleştirdi",
"{displayname} removed {path} from this conversation" => "{displayname}, bu konuşmaya {path} ekini kaldırdı",
"Search in conversations" => "Konuşmalarda arama yap",
"Add Person" => "Kişi Ekle",
"View Attached files" => "Ekli dosyalara bak",
"Chat Message" => "Sohbet İletisi",
"Files attached to this conversation" => "Bu konuşmaya eklenmiş dosyalar",
"Download " => "İndir",
"Attach more files" => "Daha fazla dosya ekle",
"Search in users" => "Kullanıcılarda ara",
"There are no other users on this ownCloud." => "Bu ownCloud üzerinde başka kullanıcı yok.",
"In order to chat please create at least one user, it will appear on the left." => "Sohbet edebilmek için lütfen en az bir kullanıcı oluşturun. Ardından sol bölümde görülecek."
);
$PLURAL_FORMS = "nplurals=2; plural=(n > 1);";
